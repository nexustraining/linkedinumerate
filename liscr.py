#!/usr/bin/env python
#
##
###
####
#####       SCRIPT THAT SCRAPES LINKEDIN DATA
######      1) get connected to someone at zeh target company
#######     2) Set zeh keywordz 
########    3) run zeh script
########    4) turn zeh names into valid emails uzing awk skillz
#########   5) pwnz them wif phisher
##########
###
### (c) Filip Waeytens 2013
###
#requirements: http://pypi.python.org/pypi/selenium#downloads
#just download the tarball and python setup.py install
# to use chrome you need to install some extra stuff, check the selenium docs
# you need to start selenium server first
#
##
### Bugs: sometimes results get repeated because of page timeouts or slowness
####    solution: adjust time.sleep to higher values    
####
 
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys
import time
import getpass
import sys
 
#setting a socks proxy in the profile for Firefox - comment out if you don't need it
 
#profile=webdriver.FirefoxProfile()
#profile.set_preference('network.proxy.type', 1)
#profile.set_preference('network.proxy.socks', '192.168.145.133')
#profile.set_preference('network.proxy.socks_port', 1080)
#print "Proxy set"
#defining the amount of max iterations
keywords=["chief","president","director","sales","marketing","network","HR","security","IT","helpdesk","project","consultant","engineer","architect","junior","freelance"]

linkedinaccount = "filip.waeytens@gmail.com"
print "Enter password for "+linkedinaccount
linkedinpassword = getpass.getpass()

f = open(sys.argv[1]+"linkedin.txt", 'w')

browser = webdriver.Firefox()
#going to the linkedin login page
browser.get("https://www.linkedin.com/uas/login") # Load page
assert "LinkedIn" in browser.title
elem = browser.find_element_by_name("session_key") 
elem.send_keys(linkedinaccount)
elem = browser.find_element_by_name("session_password")
elem.send_keys(linkedinpassword + Keys.RETURN)
time.sleep(2) # Let the page load, will be added to the API
assert "Welcome" in browser.title
browser.find_element_by_class_name("advanced-search").click()
time.sleep(2) # Let the page load, will be added to the API
assert "Advanced People Search" in browser.title
elem = browser.find_element_by_name("company") # Find the query box
elem.send_keys(sys.argv[1])
#only select people who are currently employed there
browser.find_element_by_xpath("//select[@id='currentCompany-search']/option[text()='Current']").click()
#only select first and second degree connections
browser.find_element_by_xpath("//input[@id='facet.N-id-0']").click()
browser.find_element_by_xpath("//input[@id='facet.N-id-1']").click()
browser.find_element_by_class_name("btn-primary").click()
#and Search
for keyword in keywords:
    browser.find_element_by_id("keywords-search").clear()
    elem = browser.find_element_by_id("keywords-search")
    elem.send_keys(keyword)
    time.sleep(2)
    browser.find_element_by_xpath("//input[@class='btn-primary modify-search']").click()
    time.sleep(2)
    results=browser.find_element_by_xpath("//p/strong")
    total = int(results.text.replace(",", ""))
    if total/10+1>10:    
        pages=10
    else:
        pages=total/10+1
    print "\n ----------- "+str(total)+" names found with keyword \""+keyword+"\" ---------------\n\n"
    f.write("\n\n----------- "+str(total)+" names found with keyword \""+keyword+"\" ---------------\n\n")
    
    p=1
    
    while total >=0:
        if total <11:
            total -=1
            if total <0:
                break
            blah=browser.find_element_by_xpath("//li[@id='vcard-"+str(total)+"']/div/h2/a")
            print blah.text.encode('utf-8') +":",
            f.write(blah.text.encode('utf-8') +":",)
            blah=browser.find_element_by_xpath("//li[@id='vcard-"+str(total)+"']/div/dl/dd")
            print blah.text.encode('utf-8') ,
            f.write(blah.text.encode('utf-8') ,);
            blah=browser.find_element_by_xpath("//li[@id='vcard-"+str(total)+"']/div/dl/dd[@class='connections-recommendations']")
            print "("+blah.text.encode('utf-8') +")"
            f.write(":"+blah.text.encode('utf-8')+"\n")
            #time.sleep(1)
        else: 
            i=0
            while i<10 and p<10:
                blah=browser.find_element_by_xpath("//li[@id='vcard-"+str(i)+"']/div/h2/a")
                print blah.text.encode('utf-8')+":",
                f.write(blah.text.encode('utf-8') +":",)
                blah=browser.find_element_by_xpath("//li[@id='vcard-"+str(i)+"']/div/dl/dd[@class='title']")
                print blah.text.encode('utf-8') ,
                f.write(blah.text.encode('utf-8') ,);
                blah=browser.find_element_by_xpath("//li[@id='vcard-"+str(i)+"']/div/dl/dd[@class='connections-recommendations']")
                print "("+blah.text.encode('utf-8')+")"
                f.write(":"+blah.text.encode('utf-8')+"\n")
                i += 1
                total -=1
            time.sleep(2)
            try:
                browser.find_element_by_class_name("paginator-next").click()
                time.sleep(2)
            except:
                print "can't go further than this..."
                break
            p +=1
            print "------------ PAGE "+str(p)+" -------------"
            f.write("------------ PAGE "+str(p)+" -------------\n")
            time.sleep(2)
f.close()
            
            
            
            